use modernways;

create table if not exists Taken
(
	Omschrijving varchar(500) char set utf8mb4,
    Id INT AUTO_INCREMENT PRIMARY KEY
);

create table if not exists Leden
(
	Voornaam varchar(500) char set utf8mb4,
    Id INT AUTO_INCREMENT PRIMARY KEY
);

