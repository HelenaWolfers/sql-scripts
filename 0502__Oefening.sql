use modernways;
create table if not exists huisdieren
(
	Naam varchar(100) char set utf8mb4 not null, 
    /* 
    TINYINT = whole numbers from 0 to 255;
    SMALLINT = whole numbers between -32,768 and 32,767;
    */
    Leeftijd smallint(300) not null, 
    Soort varchar(50) not null
);