create database GamePlatform;

use GamePlatform;

create table Games 
(
	Id int primary key auto_increment,
    Titel varchar(50) char set utf8mb4 not null
);

create table Platform
(
	Id int primary key auto_increment,
    Naam varchar(50) char set utf8mb4 not null
);

create table Releases
(
	Games_Id int not null,
	constraint fk_Releases_Games foreign key (Games_Id) references Games(Id),
    
    Platform_Id int not null,
    constraint fk_Releases_Platform foreign key (Platform_Id) references Platform(Id)
);