create database Twitter;

create table Gebruiker
(
	Id int primary key auto_increment, 
	Handle varchar(200)
);


create table Tweet 
(
	Id int primary key auto_increment,
    Gebruiker_id int,
    constraint fk_Tweet_Gebruiker foreign key (Gebruiker_id) references Gebruiker(Id),
    Bericht varchar(500)
);

/* TOEVOEGEN VAN DE DATA */
insert into Gebruiker(Handle)
value ('')
