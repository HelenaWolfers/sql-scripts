use modernways;

create table if not exists Metingen
(
	Tijdstip datetime not null,
    Grootte smallint(20.000) not null,
    Marge decimal(3,2) not null
);