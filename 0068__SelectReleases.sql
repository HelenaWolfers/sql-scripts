USE GamePlatform;

SELECT Games.Titel, Platform.Naam
FROM Releases
INNER JOIN Platform ON Releases.Platform_Id = Platform.Id
INNER JOIN Games ON Releases.Games_Id = Games.Id